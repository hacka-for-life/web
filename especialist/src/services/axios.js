import axios from 'axios';

export default axios.create({
    baseURL: 'https://especialist-back-end.herokuapp.com/',
});